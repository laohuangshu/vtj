import { DefineComponent } from 'vue';
import {
  XStartup,
  XIcon,
  XMenu,
  XSimpleMask,
  XAction,
  XActionBar,
  XContainer,
  XHeader,
  XPanel,
  XDialog,
  XMask,
  XField,
  XForm,
  XDialogForm,
  XTabs,
  XDataItem,
  XChart
} from './components';

export default [
  XStartup,
  XIcon,
  XMenu,
  XSimpleMask,
  XAction,
  XActionBar,
  XContainer,
  XHeader,
  XPanel,
  XDialog,
  XMask,
  XField,
  XForm,
  XDialogForm,
  XTabs,
  XDataItem,
  XChart
] as DefineComponent<any, any, any, any>[];
