import { Widget } from '../core';
export const toolbar: Array<Widget> = [
  {
    name: 'toolbar',
    type: 'Toolbar',
    region: 'toolbar'
  }
];
