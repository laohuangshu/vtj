import { withInstall } from '@vtj/ui';
import Component from './Component.vue';
export const XComponent = withInstall(Component);
export default XComponent;
export * from './component';
